// console.log("Zawarudo!");

	const Trainer = {
		name: 'Rafael Talinio',
		age: '22',
		pokemon: ['Snorlax', 'Psyduck', 'Bellsprout', 'Ekans'],
		friends: { 
			Hoenn: ['Mat','Max'],
			Kanto: ['Brock', 'Misty']
		},
		talk: function(){
			console.log(this.pokemon[0] + " I chose you!");
		}
	};
	// const pokemon1 = new Pokemon(Trainer.pokemon[0], 12);
	// const pokemon2 = new Pokemon(Trainer.pokemon[1], 50);
	// const pokemon3 = new Pokemon(Trainer.pokemon[2], 22);
	// const pokemon4 = new Pokemon(Trainer.pokemon[3], 44);

	console.log(Trainer);
	console.log("Result of dot notation:");
	console.log(Trainer.name);
	console.log("Result of bracket notation:");
	console.log(Trainer['pokemon']);
	console.log(Trainer.talk());

	function Pokemon(name, level){
		// Pokemon Method
		this.pokemonName = name;
		this.pokemonLevel = level;
		this.pokemonHealth = 2 * level;
		this.pokemonAttack = level;

		/* method */
		this.attack = function(targetPokemon){
			console.log(this.pokemonName + ' tackles ' + targetPokemon.pokemonName);
			const targetPokemonAfterAttack = targetPokemon.pokemonHealth - this.pokemonAttack;
			console.log(targetPokemon.pokemonName + " health is now reduced to " + targetPokemonAfterAttack)

			targetPokemon.pokemonHealth -= this.pokemonAttack;
			if(targetPokemonAfterAttack <= 0){
				console.log(targetPokemon.pokemonName + " Fainted!");
			};

		};
			
	}
	
	const snorlax = new Pokemon(Trainer.pokemon[0], 12);
	console.log(snorlax);

	let bellsprout = new Pokemon(Trainer.pokemon[2], 50);
	console.log(bellsprout);
	snorlax.attack(bellsprout);
	snorlax.attack(bellsprout);
	snorlax.attack(bellsprout);
	snorlax.attack(bellsprout);
	snorlax.attack(bellsprout);
	snorlax.attack(bellsprout);
	snorlax.attack(bellsprout);
	snorlax.attack(bellsprout);
	snorlax.attack(bellsprout);


	